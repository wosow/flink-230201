package com.atguigu.app;

import java.sql.*;

public class MysqlTest {

    public static void main(String[] args) throws Exception {

        //获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql://hadoop102:3306/test?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=UTF-8", "root", "000000");

        //预编译SQL语句
        PreparedStatement preparedStatement = connection.prepareStatement("select * from kafka_ws1 where stt='0' and edt='10' and id ='1002'");

        //执行查询
        ResultSet resultSet = preparedStatement.executeQuery();

        //遍历打印
        while (resultSet.next()) {
            System.out.println("aaa");
            System.out.println("STT:" + resultSet.getObject(1) +
                    ",EDT:" + resultSet.getObject(2) +
                    ",ID:" + resultSet.getObject(3) +
                    ",VC:" + resultSet.getObject(4));
        }

        //关闭资源
        resultSet.close();
        preparedStatement.close();
        connection.close();

    }

}
