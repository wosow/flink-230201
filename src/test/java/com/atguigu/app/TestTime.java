package com.atguigu.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestTime {

    public static void main(String[] args) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.parse("2023-07-01 15:42:15").getTime());

    }

}
