package com.atguigu.func;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.table.functions.TableAggregateFunction;
import org.apache.flink.util.Collector;

public class MyUDTAF extends TableAggregateFunction<Double, Tuple2<Double, Double>> {

    @Override
    public Tuple2<Double, Double> createAccumulator() {
        return new Tuple2<>(Double.MIN_VALUE, Double.MIN_VALUE);
    }

    public void accumulate(Tuple2<Double, Double> acc, Double value) {
        if (value > acc.f0) {
            acc.f1 = acc.f0;
            acc.f0 = value;
        } else if (value > acc.f1) {
            acc.f1 = value;
        }
    }

    public void merge(Tuple2<Double, Double> acc, Iterable<Tuple2<Double, Double>> it) {
        for (Tuple2<Double, Double> tuple2 : it) {
            accumulate(acc, tuple2.f0);
            accumulate(acc, tuple2.f1);
        }
    }

    public void emitValue(Tuple2<Double, Double> acc, Collector<Double> out) {
        out.collect(acc.f0);
        if (acc.f1 != Double.MIN_VALUE) {
            out.collect(acc.f1);
        }
    }
}
