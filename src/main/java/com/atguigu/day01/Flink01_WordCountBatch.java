package com.atguigu.day01;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.operators.*;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

public class Flink01_WordCountBatch {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        //读取文件数据   按行读取
        DataSource<String> textFile = env.readTextFile("input/word.txt");

        //切分单词,压平
        FlatMapOperator<String, String> wordDS = textFile.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public void flatMap(String value, Collector<String> out) throws Exception {
                String[] words = value.split(" ");
                for (String word : words) {
                    out.collect(word);
                }
            }
        });

        //将单个单次转换为word -> (word,1)
        MapOperator<String, Tuple2<String, Long>> wordToOneDS = wordDS.map(new MapFunction<String, Tuple2<String, Long>>() {
            @Override
            public Tuple2<String, Long> map(String value) throws Exception {
                return new Tuple2<>(value, 1L);
            }
        });

        //分组数据
        UnsortedGrouping<Tuple2<String, Long>> groupByWordDS = wordToOneDS.groupBy(0);

        //聚合数据
        AggregateOperator<Tuple2<String, Long>> sum = groupByWordDS.sum(1);

        //打印
        sum.print();

    }
}
