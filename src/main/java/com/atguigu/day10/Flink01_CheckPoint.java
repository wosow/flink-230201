package com.atguigu.day10;

import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink01_CheckPoint {

    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //开启CheckPoint  生产环境中,起码为分钟级 10
        env.enableCheckpointing(5000L);

        CheckpointConfig checkpointConfig = env.getCheckpointConfig();

        //CheckPoint触发的间隔时间
        checkpointConfig.setCheckpointInterval(5000L);

        //指定远程状态的保存位置,一般选择HDFS
        checkpointConfig.setCheckpointStorage("");

        //指定CheckPoint的超时时间
        checkpointConfig.setCheckpointTimeout(10000L);

        //一致性语义
        checkpointConfig.setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);

        //最多同时可以存在的CheckPoint的数量
        checkpointConfig.setMaxConcurrentCheckpoints(2);

        //两次CheckPoint之间的间隔时间
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);

        //指的是在主动Cancel任务时,是否保留CheckPoint结果
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);



    }

}
