package com.atguigu.day02;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.RestOptions;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.LocalStreamEnvironment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Flink02_P {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取流执行环境
//        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        Configuration configuration = new Configuration();
        configuration.setInteger(RestOptions.PORT, 8081);
        LocalStreamEnvironment env = StreamExecutionEnvironment.createLocalEnvironment(configuration);
        //env.disableOperatorChaining();

        //TODO 2.从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //TODO 3.压平
        SingleOutputStreamOperator<String> wordDS = socketTextStream.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public void flatMap(String value, Collector<String> out) throws Exception {
                String[] words = value.split(" ");
                for (String word : words) {
                    out.collect(word);
                }
            }
        });

        //TODO 4.将数据转换为元组
        SingleOutputStreamOperator<Tuple2<String, Long>> wordToOneDS = wordDS
                .map(word -> new Tuple2<String, Long>(word, 1L))
                .returns(Types.TUPLE(Types.STRING, Types.LONG))
                //.startNewChain()
                .slotSharingGroup("g1");

        //TODO 5.分组
        KeyedStream<Tuple2<String, Long>, String> keyedStream = wordToOneDS.keyBy(new KeySelector<Tuple2<String, Long>, String>() {
            @Override
            public String getKey(Tuple2<String, Long> value) throws Exception {
                return value.f0;
            }
        });

        //TODO 6.聚合
        SingleOutputStreamOperator<Tuple2<String, Long>> sum = keyedStream.sum(1);

        //TODO 7.打印
        sum.print("env>>>");

        //TODO 8.启动
        env.execute();

    }
}
