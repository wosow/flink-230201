package com.atguigu.day07;

import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;

public class Flink08_KeyedProcessFunction_TopN {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口获取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为二元组
        SingleOutputStreamOperator<Tuple2<Double, Integer>> vcAndOneDS = socketTextStream
                .assignTimestampsAndWatermarks(WatermarkStrategy.<String>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<String>() {
                    @Override
                    public long extractTimestamp(String element, long recordTimestamp) {
                        String[] split = element.split(",");
                        return Long.parseLong(split[1]);
                    }
                }))
                .map(new MapFunction<String, Tuple2<Double, Integer>>() {
                    @Override
                    public Tuple2<Double, Integer> map(String value) throws Exception {
                        String[] split = value.split(",");
                        return new Tuple2<>(Double.parseDouble(split[2]), 1);
                    }
                });

        //分组、开窗、聚合
        SingleOutputStreamOperator<Tuple4<Long, Long, Double, Integer>> reduceDS = vcAndOneDS.keyBy(new KeySelector<Tuple2<Double, Integer>, Double>() {
                    @Override
                    public Double getKey(Tuple2<Double, Integer> value) throws Exception {
                        return value.f0;
                    }
                }).window(TumblingEventTimeWindows.of(Time.seconds(10)))
                .reduce(new ReduceFunction<Tuple2<Double, Integer>>() {
                    @Override
                    public Tuple2<Double, Integer> reduce(Tuple2<Double, Integer> value1, Tuple2<Double, Integer> value2) throws Exception {
                        return new Tuple2<>(value1.f0, value1.f1 + value2.f1);
                    }
                }, new WindowFunction<Tuple2<Double, Integer>, Tuple4<Long, Long, Double, Integer>, Double, TimeWindow>() {
                    @Override
                    public void apply(Double aDouble, TimeWindow window, Iterable<Tuple2<Double, Integer>> input, Collector<Tuple4<Long, Long, Double, Integer>> out) throws Exception {
                        Tuple2<Double, Integer> next = input.iterator().next();
                        out.collect(new Tuple4<>(window.getStart(),
                                window.getEnd(),
                                next.f0,
                                next.f1));
                    }
                });

        //按照窗口信息重新分组
        SingleOutputStreamOperator<String> resultDS = reduceDS.keyBy(new KeySelector<Tuple4<Long, Long, Double, Integer>, String>() {
            @Override
            public String getKey(Tuple4<Long, Long, Double, Integer> value) throws Exception {
                return value.f0 + "-" + value.f1;
            }
        }).process(new KeyedProcessFunction<String, Tuple4<Long, Long, Double, Integer>, String>() {

            private ArrayList<Tuple2<Double, Integer>> list = new ArrayList<>();

            @Override
            public void processElement(Tuple4<Long, Long, Double, Integer> value, KeyedProcessFunction<String, Tuple4<Long, Long, Double, Integer>, String>.Context ctx, Collector<String> out) throws Exception {
                //注册定时器
//                if (list.size() == 0) {
                TimerService timerService = ctx.timerService();
                timerService.registerEventTimeTimer(value.f1 + 10);
//                }
                //将数据放入集合
                list.add(new Tuple2<>(value.f2, value.f3));
            }

            @Override
            public void onTimer(long timestamp, KeyedProcessFunction<String, Tuple4<Long, Long, Double, Integer>, String>.OnTimerContext ctx, Collector<String> out) throws Exception {

                list.sort(new Comparator<Tuple2<Double, Integer>>() {
                    @Override
                    public int compare(Tuple2<Double, Integer> o1, Tuple2<Double, Integer> o2) {
                        //从大到小排序
                        return o2.f1 - o1.f1;
                    }
                });

                StringBuilder stringBuilder = new StringBuilder();
                //输出
                for (int i = 0; i < Math.min(2, list.size()); i++) {
                    Tuple2<Double, Integer> vcCount = list.get(i);
                    stringBuilder.append("Top" + (i + 1) + ":Vc:" + vcCount.f0 + ",Count:" + vcCount.f1);
                    stringBuilder.append("\n");
                }

                //输出结果
                out.collect(stringBuilder.toString());

                //清空集合
                list.clear();
            }
        });

        //打印结果
        resultDS.print();

        //启动任务
        env.execute();
    }
}
