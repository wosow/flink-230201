package com.atguigu.day07;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.Iterator;

public class Flink05_WindowProcessFunction {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //开窗、聚合
        SingleOutputStreamOperator<WaterSensor> windowAllDS = waterSensorDS.windowAll(TumblingProcessingTimeWindows.of(Time.seconds(10)))
                .process(new ProcessAllWindowFunction<WaterSensor, WaterSensor, TimeWindow>() {
                    @Override
                    public void process(ProcessAllWindowFunction<WaterSensor, WaterSensor, TimeWindow>.Context context, Iterable<WaterSensor> elements, Collector<WaterSensor> out) throws Exception {
                        TimeWindow window = context.window();

                        Double maxVc = 0.0D;

                        Iterator<WaterSensor> iterator = elements.iterator();
                        while (iterator.hasNext()) {
                            WaterSensor next = iterator.next();
                            maxVc = Math.max(maxVc, next.getVc());
                        }

                        out.collect(new WaterSensor(null, System.currentTimeMillis(), maxVc));
                    }
                });

        //分组、开窗、聚合
        SingleOutputStreamOperator<Tuple2<String, Double>> windowDS = waterSensorDS.keyBy(WaterSensor::getId)
                .window(TumblingProcessingTimeWindows.of(Time.seconds(10)))
                .process(new ProcessWindowFunction<WaterSensor, Tuple2<String, Double>, String, TimeWindow>() {
                    @Override
                    public void process(String key, ProcessWindowFunction<WaterSensor, Tuple2<String, Double>, String, TimeWindow>.Context context, Iterable<WaterSensor> elements, Collector<Tuple2<String, Double>> out) throws Exception {
                        Double maxVc = 0.0D;

                        Iterator<WaterSensor> iterator = elements.iterator();
                        while (iterator.hasNext()) {
                            WaterSensor next = iterator.next();
                            maxVc = Math.max(maxVc, next.getVc());
                        }

                        out.collect(new Tuple2<>(key, maxVc));
                    }
                });

        //打印
        windowAllDS.print("windowAllDS>>>>");
        windowDS.print("windowDS>>>>>");

        //启动任务
        env.execute();

    }

}
