package com.atguigu.day07;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

public class Flink04_KeyedProcessFunction {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //读取端口数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS
                .assignTimestampsAndWatermarks(WatermarkStrategy.<WaterSensor>forMonotonousTimestamps().withTimestampAssigner(new SerializableTimestampAssigner<WaterSensor>() {
                    @Override
                    public long extractTimestamp(WaterSensor element, long recordTimestamp) {
                        return element.getTs();
                    }
                }))
                .keyBy(WaterSensor::getId);

        //定时器功能
        SingleOutputStreamOperator<WaterSensor> processDS = keyedStream.process(new KeyedProcessFunction<String, WaterSensor, WaterSensor>() {
            @Override
            public void processElement(WaterSensor value, KeyedProcessFunction<String, WaterSensor, WaterSensor>.Context ctx, Collector<WaterSensor> out) throws Exception {

                //获取时间服务对象
                TimerService timerService = ctx.timerService();
                //注册处理时间定时器
                long processingTime = timerService.currentProcessingTime();
                long processTs = processingTime + 10000L;
                System.out.println("注册一个处理时间为:" + processTs + "的定时器！");
                timerService.registerProcessingTimeTimer(processTs);

                //注册事件时间定时器
                long currentWatermark = timerService.currentWatermark();
                long eventTs = currentWatermark + 10000L;
                System.out.println("注册一个事件时间为:" + eventTs + "的定时器！");
                timerService.registerEventTimeTimer(eventTs);

                //删除定时器 一定要与注册的时间完全一致
//                timerService.deleteProcessingTimeTimer(processingTime + 10000L);
//                timerService.deleteEventTimeTimer(currentWatermark + 10000L);

                out.collect(value);
            }

            //定时器触发
            @Override
            public void onTimer(long timestamp, KeyedProcessFunction<String, WaterSensor, WaterSensor>.OnTimerContext ctx, Collector<WaterSensor> out) throws Exception {
                System.out.println("定时器触发：" + timestamp);
            }
        });

        //打印输出
        processDS.print();

        //启动
        env.execute();

    }

}
