package com.atguigu.day07;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.connector.jdbc.JdbcStatementBuilder;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.kafka.common.metrics.stats.Max;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

public class Flink01_Kafka_Max_Mysql {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从Kafka读取数据,同时提取时间戳生成WaterMark
        KafkaSource<String> kafkaSource = KafkaSource.<String>builder()
                .setBootstrapServers("hadoop102:9092")
                .setTopics("test")
                .setStartingOffsets(OffsetsInitializer.latest())
                .setGroupId("kafka_max_mysql")
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        DataStreamSource<String> kafkaDS = env.fromSource(kafkaSource,
                WatermarkStrategy.<String>forMonotonousTimestamps().withTimestampAssigner(new SerializableTimestampAssigner<String>() {
                    @Override
                    public long extractTimestamp(String element, long recordTimestamp) {
                        String[] split = element.split(",");
                        return Long.parseLong(split[1]);
                    }
                }), "kafka-source");

        //将数据转换为JavaBean
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = kafkaDS.map(new StringToWaterSensor());

        //按照id进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //开窗聚合
        SingleOutputStreamOperator<Tuple4<String, String, String, Double>> resultDS = keyedStream.window(TumblingEventTimeWindows.of(Time.seconds(10)))
                .reduce(new ReduceFunction<WaterSensor>() {
                    @Override
                    public WaterSensor reduce(WaterSensor value1, WaterSensor value2) throws Exception {
                        return new WaterSensor(value1.getId(),
                                0L,
                                Math.max(value1.getVc(), value2.getVc()));
                    }
                }, new WindowFunction<WaterSensor, Tuple4<String, String, String, Double>, String, TimeWindow>() {
                    @Override
                    public void apply(String key, TimeWindow window, Iterable<WaterSensor> input, Collector<Tuple4<String, String, String, Double>> out) throws Exception {

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        WaterSensor waterSensor = input.iterator().next();

                        out.collect(new Tuple4<>(sdf.format(window.getStart()),
                                sdf.format(window.getEnd()),
                                key,
                                waterSensor.getVc()));
                    }
                });

        //将数据写出到MySQL
        resultDS.addSink(JdbcSink.<Tuple4<String, String, String, Double>>sink("insert into kafka_ws values(?,?,?,?)",
                new JdbcStatementBuilder<Tuple4<String, String, String, Double>>() {
                    @Override
                    public void accept(PreparedStatement preparedStatement, Tuple4<String, String, String, Double> value) throws SQLException {
                        preparedStatement.setObject(1, value.f0);
                        preparedStatement.setObject(2, value.f1);
                        preparedStatement.setObject(3, value.f2);
                        preparedStatement.setObject(4, value.f3);
                    }
                }, new JdbcExecutionOptions.Builder()
                        .withBatchSize(1)
                        .build(),
                new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                        //.withDriverName("")
                        .withUrl("jdbc:mysql://hadoop102:3306/test?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=UTF-8")
                        .withUsername("root")
                        .withPassword("000000")
                        .build()));

        resultDS.print(">>>>>");

        //启动任务
        env.execute();

    }

}
