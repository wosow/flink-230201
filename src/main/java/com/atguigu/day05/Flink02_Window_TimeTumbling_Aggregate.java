package com.atguigu.day05;

import com.atguigu.bean.Acc;
import com.atguigu.bean.WaterSensor;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;

public class Flink02_Window_TimeTumbling_Aggregate {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new MapFunction<String, WaterSensor>() {
            @Override
            public WaterSensor map(String value) throws Exception {
                String[] split = value.split(",");
                return new WaterSensor(split[0], Long.parseLong(split[1]), Double.parseDouble(split[2]));
            }
        });

        //按照id进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //开窗 10秒的滚动窗口
        WindowedStream<WaterSensor, String, TimeWindow> windowedStream = keyedStream.window(TumblingProcessingTimeWindows.of(Time.seconds(10)));

        //聚合,计算每个窗口中的VC的均值
        SingleOutputStreamOperator<Tuple2<String, Double>> aggregate = windowedStream.aggregate(new AggregateFunction<WaterSensor, Acc, Tuple2<String, Double>>() {
            @Override
            public Acc createAccumulator() {
                return new Acc(null, 0.0D, 0L);
            }

            @Override
            public Acc add(WaterSensor value, Acc accumulator) {
                if (accumulator.getId() == null) {
                    accumulator.setId(value.getId());
                }
                accumulator.setVc(accumulator.getVc() + value.getVc());
                accumulator.setCount(accumulator.getCount() + 1L);
                return accumulator;
            }

            @Override
            public Tuple2<String, Double> getResult(Acc accumulator) {
                return new Tuple2<>(accumulator.getId(), accumulator.getVc() / accumulator.getCount());
            }

            @Override
            public Acc merge(Acc a, Acc b) {
                a.setVc(a.getVc() + b.getVc());
                a.setCount(a.getCount() + b.getCount());
                return a;
            }
        });

        //打印
        aggregate.print();

        //启动
        env.execute();

    }

}
