package com.atguigu.day12;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import static org.apache.flink.table.api.Expressions.$;

public class Flink01_SQL_Test {

    public static void main(String[] args) {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //获取表执行环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //创建表
        tableEnv.executeSql("" +
                "CREATE TABLE kafka_table_pt (\n" +
                "  `id` STRING,\n" +
                "  `ts` BIGINT,\n" +
                "  `vc` DOUBLE,\n" +
                "  `pt` AS PROCTIME()\n" +
                ") WITH (\n" +
                "  'connector' = 'kafka',\n" +
                "  'topic' = 'test',\n" +
                "  'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
                "  'properties.group.id' = 'test_230201',\n" +
                "  'scan.startup.mode' = 'latest-offset',\n" +
                "  'format' = 'csv'\n" +
                ")");

        //执行查询
        Table t1 = tableEnv.sqlQuery("select id,sum(vc) vc from kafka_table_pt group by id");

        //创建临时视图
        tableEnv.createTemporaryView("t_1", t1);
        tableEnv.sqlQuery("select * from t_1");

        //使用TableAPI实现查询
        Table select = t1.where($("vc").isGreater("30.0D"))
                .select($("id"), $("vc"));
        tableEnv.sqlQuery("select id,vc from t_1 where vc>30.0");

        //输出数据
        tableEnv.executeSql("insert into t2 select id,sum(vc) vc from kafka_table_pt group by id");
        t1.insertInto("t2");

        //打印
        t1.execute().print();

    }

}
