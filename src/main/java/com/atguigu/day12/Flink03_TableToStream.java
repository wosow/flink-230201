package com.atguigu.day12;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

public class Flink03_TableToStream {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //从Kafka创建表
        tableEnv.executeSql("" +
                "CREATE TABLE kafka_table_pt (\n" +
                "  `id` STRING,\n" +
                "  `ts` BIGINT,\n" +
                "  `vc` DOUBLE\n" +
                //"  `pt` AS PROCTIME()\n" +
                ") WITH (\n" +
                "  'connector' = 'kafka',\n" +
                "  'topic' = 'test',\n" +
                "  'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
                "  'properties.group.id' = 'test_230201',\n" +
                "  'scan.startup.mode' = 'latest-offset',\n" +
                "  'format' = 'csv'\n" +
                ")");

        //追加查询
//        Table table = tableEnv.sqlQuery("select * from kafka_table_pt");
//        DataStream<WaterSensor> waterSensorDataStream = tableEnv.toDataStream(table, WaterSensor.class);
//        waterSensorDataStream.print();

        //聚合查询
        Table table = tableEnv.sqlQuery("select id,max(ts) ts,sum(vc) vc from kafka_table_pt group by id");
        DataStream<Row> rowDataStream = tableEnv.toChangelogStream(table);
        DataStream<Tuple2<Boolean, WaterSensor>> tuple2DataStream = tableEnv.toRetractStream(table, WaterSensor.class);
        tuple2DataStream.print();

        //启动任务
        env.execute();
    }

}
