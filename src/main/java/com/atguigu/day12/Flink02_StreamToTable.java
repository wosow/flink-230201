package com.atguigu.day12;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Flink02_StreamToTable {

    public static void main(String[] args) {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //从端口读取数据创建流
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将流转换为JavaBean
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //将流转换为动态表 1 必须为JavaBean
        //tableEnv.createTemporaryView("ws1", waterSensorDS);
        //执行查询
        //tableEnv.sqlQuery("select * from ws1").execute().print();

        //将流转换为动态表 2 最好为JavaBean
        Table table = tableEnv.fromDataStream(waterSensorDS);
        tableEnv.createTemporaryView("ws2", table);
        //执行查询
        tableEnv.sqlQuery("select id,vc from ws2").execute().print();

    }

}
