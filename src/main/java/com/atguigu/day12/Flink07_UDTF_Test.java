package com.atguigu.day12;

import com.atguigu.func.MyUDTF;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Flink07_UDTF_Test {

    public static void main(String[] args) {

//        Class<WaterSensor> waterSensorClass = WaterSensor.class;
//        Method[] declaredMethods = waterSensorClass.getDeclaredMethods();
//        for (Method declaredMethod : declaredMethods) {
//            if (declaredMethod.getName().equals("getId")) {
//                declaredMethod.invoke()
//            }
//        }
//        Field[] declaredFields = waterSensorClass.getDeclaredFields();
//        for (Field declaredField : declaredFields) {
//            if (declaredField.getName().equals("id")){
//                declaredField.get();
//            }
//        }

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //获取表执行环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //创建表
        tableEnv.executeSql("" +
                "CREATE TABLE kafka_table_pt (\n" +
                "  `id` STRING,\n" +
                "  `ts` BIGINT,\n" +
                "  `vc` DOUBLE,\n" +
                "  `pt` AS PROCTIME()\n" +
                ") WITH (\n" +
                "  'connector' = 'kafka',\n" +
                "  'topic' = 'test',\n" +
                "  'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
                "  'properties.group.id' = 'test_230201',\n" +
                "  'scan.startup.mode' = 'latest-offset',\n" +
                "  'format' = 'csv'\n" +
                ")");

        //注册自定义函数
        tableEnv.createTemporarySystemFunction("my_udtf", MyUDTF.class);

        //使用函数
        tableEnv.sqlQuery("" +
                        "SELECT \n" +
                        "    id, \n" +
                        "    vc,\n" +
                        "    word1 \n" +
                        "FROM kafka_table_pt, LATERAL TABLE(my_udtf(id))")
                .execute()
                .print();

    }

}
