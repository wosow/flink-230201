package com.atguigu.day08;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.connector.jdbc.JdbcStatementBuilder;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.*;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Duration;

public class Flink03_Kafka_Sum_Mysql {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从Kafka读取数据,同时提取时间戳生成WaterMark
        KafkaSource<String> kafkaSource = KafkaSource.<String>builder()
                .setBootstrapServers("hadoop102:9092")
                .setTopics("test")
                .setStartingOffsets(OffsetsInitializer.latest())
                .setGroupId("kafka_max_mysql")
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        DataStreamSource<String> kafkaDS = env.fromSource(kafkaSource,
                WatermarkStrategy.<String>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<String>() {
                    @Override
                    public long extractTimestamp(String element, long recordTimestamp) {
                        String[] split = element.split(",");
                        return Long.parseLong(split[1]);
                    }
                }), "kafka-source");

        //将数据转换为JavaBean
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = kafkaDS.map(new StringToWaterSensor());

        //按照id进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //开窗聚合
        OutputTag<WaterSensor> outputTag = new OutputTag<WaterSensor>("side") {
        };
        SingleOutputStreamOperator<Tuple4<String, String, String, Double>> resultDS = keyedStream.window(TumblingEventTimeWindows.of(Time.seconds(10)))
                .allowedLateness(Time.seconds(2))
                .sideOutputLateData(outputTag)
                .reduce(new ReduceFunction<WaterSensor>() {
                    @Override
                    public WaterSensor reduce(WaterSensor value1, WaterSensor value2) throws Exception {
                        return new WaterSensor(value1.getId(),
                                0L,
                                value1.getVc() + value2.getVc());
                    }
                }, new WindowFunction<WaterSensor, Tuple4<String, String, String, Double>, String, TimeWindow>() {
                    @Override
                    public void apply(String key, TimeWindow window, Iterable<WaterSensor> input, Collector<Tuple4<String, String, String, Double>> out) throws Exception {

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        WaterSensor waterSensor = input.iterator().next();

                        out.collect(new Tuple4<>(sdf.format(window.getStart()),
                                sdf.format(window.getEnd()),
                                key,
                                waterSensor.getVc()));
                    }
                });


        //打印主流
        resultDS.print("resultDS>>>>>");

        //获取侧输出流并打印
        SideOutputDataStream<WaterSensor> sideOutput = resultDS.getSideOutput(outputTag);
        sideOutput.print("sideOutput>>>>");

        SingleOutputStreamOperator<Tuple4<String, String, String, Double>> mapDS = sideOutput.map(new MapFunction<WaterSensor, Tuple4<String, String, String, Double>>() {
            @Override
            public Tuple4<String, String, String, Double> map(WaterSensor value) throws Exception {

                Long ts = value.getTs();
                long start = TimeWindow.getWindowStartWithOffset(ts, 0L, 10000);
                long end = start + 10000L;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                return new Tuple4<>(sdf.format(start),
                        sdf.format(end),
                        value.getId(),
                        value.getVc());
            }
        });

        //将主流数据写出到MySQL  有则更新，无则插入
        resultDS.addSink(JdbcSink.<Tuple4<String, String, String, Double>>sink("INSERT INTO kafka_ws1 VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE `vc` = ?",
                new JdbcStatementBuilder<Tuple4<String, String, String, Double>>() {
                    @Override
                    public void accept(PreparedStatement preparedStatement, Tuple4<String, String, String, Double> value) throws SQLException {
                        preparedStatement.setObject(1, value.f0);
                        preparedStatement.setObject(2, value.f1);
                        preparedStatement.setObject(3, value.f2);
                        preparedStatement.setObject(4, value.f3);
                        preparedStatement.setObject(5, value.f3);
                    }
                }, new JdbcExecutionOptions.Builder()
                        .withBatchSize(1)
                        .build(),
                new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                        //.withDriverName("")
                        .withUrl("jdbc:mysql://hadoop102:3306/test?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=UTF-8")
                        .withUsername("root")
                        .withPassword("000000")
                        .build()));

        //将侧输出流写出到MySQL  累加到数据上
        mapDS.addSink(JdbcSink.<Tuple4<String, String, String, Double>>sink("INSERT INTO kafka_ws1 VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE `vc` = vc+?",
                new JdbcStatementBuilder<Tuple4<String, String, String, Double>>() {
                    @Override
                    public void accept(PreparedStatement preparedStatement, Tuple4<String, String, String, Double> value) throws SQLException {
                        preparedStatement.setObject(1, value.f0);
                        preparedStatement.setObject(2, value.f1);
                        preparedStatement.setObject(3, value.f2);
                        preparedStatement.setObject(4, value.f3);
                        preparedStatement.setObject(5, value.f3);
                    }
                }, new JdbcExecutionOptions.Builder()
                        .withBatchSize(1)
                        .build(),
                new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                        //.withDriverName("")
                        .withUrl("jdbc:mysql://hadoop102:3306/test?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=UTF-8")
                        .withUsername("root")
                        .withPassword("000000")
                        .build()));

        //启动任务
        env.execute();

    }

}
