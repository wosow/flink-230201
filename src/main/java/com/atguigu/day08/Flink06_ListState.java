package com.atguigu.day08;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.ArrayList;
import java.util.Comparator;

public class Flink06_ListState {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //使用状态编程输出:每种传感器输出最高的3个水位值
        SingleOutputStreamOperator<String> resultDS = keyedStream.map(new RichMapFunction<WaterSensor, String>() {

            //声明状态
            private ListState<Double> listState;

            @Override
            public void open(Configuration parameters) throws Exception {
                listState = getRuntimeContext().getListState(new ListStateDescriptor<Double>("list-state", Double.class));
            }

            @Override
            public String map(WaterSensor value) throws Exception {

                //将数据的VC放入状态
                listState.add(value.getVc());

                //取出状态中的数据做排序
                Iterable<Double> iterable = listState.get();
                ArrayList<Double> list = new ArrayList<>();
                for (Double lastVc : iterable) {
                    list.add(lastVc);
                }
                list.sort(new Comparator<Double>() {
                    @Override
                    public int compare(Double o1, Double o2) {
                        return o2.compareTo(o1);
                    }
                });

                //如果集合超过3个
                int size = list.size();
                if (size > 3) {
                    list.remove(3);
                }

                //将状态更新
                listState.update(list);

                //输出结果数据
                StringBuilder stringBuilder = new StringBuilder(value.getId()).append("最大的三个VC为：");
                for (int i = 0; i < list.size(); i++) {
                    Double vc = list.get(i);
                    stringBuilder.append(vc);
                    //判断是否为最后一条数据
                    if (i == list.size() - 1) {
                        stringBuilder.append("。");
                    } else {
                        stringBuilder.append(",");
                    }
                }
                return stringBuilder.toString();
            }
        });

        //打印
        resultDS.print();

        //启动
        env.execute();

    }

}
