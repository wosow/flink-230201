package com.atguigu.day08;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink04_ValueState {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //使用状态编程计算:如果同一个传感器连续两条数据都超过10,则输出警告信息！
        SingleOutputStreamOperator<String> resultDS = keyedStream.map(new RichMapFunction<WaterSensor, String>() {

            //声明状态
            private ValueState<Double> valueState;

            @Override
            public void open(Configuration parameters) throws Exception {
                //初始化状态
                valueState = getRuntimeContext().getState(new ValueStateDescriptor<Double>("value-state", Double.class));
            }

            @Override
            public String map(WaterSensor value) throws Exception {

                //获取状态中的数据
                Double lastVc = valueState.value(); //取值,默认值为null
                //valueState.update(20.0D);         写值
                //valueState.clear();               删值

                //获取当前数据中的VC
                Double curVc = value.getVc();

//                if (lastVc == null) {
//                    valueState.update(curVc);
//                } else if (lastVc > 10.0D && curVc > 10.0D) {
//                    valueState.update(curVc);
//                    return "报警信息！";
//                } else {
//                    valueState.update(curVc);
//                }

                valueState.update(curVc);
                if (lastVc != null && lastVc > 10.0D && curVc > 10.0D) {
                    return value + "报警信息:" + value.getId() + "连续两条水位超过10！";
                } else {
                    return value.toString();
                }
            }
        });

        //打印
        resultDS.print();

        //启动任务
        env.execute();

    }

}
