package com.atguigu.day06;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.OutputTag;

import java.time.Duration;

public class Flink02_Window_EventTimeTumbling_Late {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new MapFunction<String, WaterSensor>() {
            @Override
            public WaterSensor map(String value) throws Exception {
                String[] split = value.split(",");
                return new WaterSensor(split[0], Long.parseLong(split[1]), Double.parseDouble(split[2]));
            }
        });

        //提取时间戳生成WaterMark
        SingleOutputStreamOperator<WaterSensor> waterSensorWithWMDS = waterSensorDS
//                .assignTimestampsAndWatermarks(WatermarkStrategy.<WaterSensor>forMonotonousTimestamps()
                .assignTimestampsAndWatermarks(WatermarkStrategy.<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(5))
                        .withTimestampAssigner(new SerializableTimestampAssigner<WaterSensor>() {
                            @Override
                            public long extractTimestamp(WaterSensor element, long recordTimestamp) {
                                return element.getTs();
                            }
                        }));

        //按照id进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorWithWMDS.keyBy(WaterSensor::getId);

//        SingleOutputStreamOperator<WaterSensor> waterSensorSingleOutputStreamOperator = keyedStream.assignTimestampsAndWatermarks();
//        waterSensorSingleOutputStreamOperator.

        //开窗 10秒的滚动窗口
        OutputTag<WaterSensor> outputTag = new OutputTag<WaterSensor>("Late") {
        };
//        OutputTag<WaterSensor> outputTag2 = new OutputTag<WaterSensor>("Late");
        WindowedStream<WaterSensor, String, TimeWindow> windowedStream = keyedStream
                .window(TumblingEventTimeWindows.of(Time.seconds(10)))
                .allowedLateness(Time.seconds(5))
                .sideOutputLateData(outputTag)
                ;

        //聚合,计算每个窗口中的最大VC
        SingleOutputStreamOperator<Tuple2<String, Long>> sumDS = windowedStream.aggregate(new AggregateFunction<WaterSensor, Tuple2<String, Long>, Tuple2<String, Long>>() {
            @Override
            public Tuple2<String, Long> createAccumulator() {
                return new Tuple2<>(null, 0L);
            }

            @Override
            public Tuple2<String, Long> add(WaterSensor value, Tuple2<String, Long> accumulator) {
                if (accumulator.f0 == null) {
                    accumulator.f0 = value.getId();
                }
                accumulator.f1 = accumulator.f1 + 1L;
                return accumulator;
            }

            @Override
            public Tuple2<String, Long> getResult(Tuple2<String, Long> accumulator) {
                return accumulator;
            }

            @Override
            public Tuple2<String, Long> merge(Tuple2<String, Long> a, Tuple2<String, Long> b) {
                a.f1 = a.f1 + b.f1;
                return a;
            }
        });

        //获取侧输出流数据并打印
        sumDS.getSideOutput(outputTag).print("SideOut>>>>");

        //打印
        sumDS.print("sumDS>>>>>");

        //启动
        env.execute();

    }

}
