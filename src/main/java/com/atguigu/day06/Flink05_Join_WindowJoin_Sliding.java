package com.atguigu.day06;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

public class Flink05_Join_WindowJoin_Sliding {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口获取数据
        //1001,banzhang
        DataStreamSource<String> socketTextStream1 = env.socketTextStream("hadoop102", 8888);
        //1001,male
        DataStreamSource<String> socketTextStream2 = env.socketTextStream("hadoop102", 9999);

        //滚动窗口JOIN
        DataStream<Tuple2<String, String>> joinDS = socketTextStream1.join(socketTextStream2)
                .where(new KeySelector<String, String>() {
                    @Override
                    public String getKey(String value) throws Exception {
                        return value.split(",")[0];
                    }
                }).equalTo(new KeySelector<String, String>() {
                    @Override
                    public String getKey(String value) throws Exception {
                        return value.split(",")[0];
                    }
                }).window(SlidingProcessingTimeWindows.of(Time.seconds(10), Time.seconds(5)))
                .apply(new JoinFunction<String, String, Tuple2<String, String>>() {
                    @Override
                    public Tuple2<String, String> join(String first, String second) throws Exception {
                        return new Tuple2<>(first, second);
                    }
                });

        //打印
        joinDS.print();

        //启动
        env.execute();

    }

}
