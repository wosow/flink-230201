package com.atguigu.day11;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Flink01_DistinctVc {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //去重数据
        SingleOutputStreamOperator<WaterSensor> resultDS = keyedStream.flatMap(new RichFlatMapFunction<WaterSensor, WaterSensor>() {

            private MapState<Double, String> mapState;

            @Override
            public void open(Configuration parameters) throws Exception {
                mapState = getRuntimeContext().getMapState(new MapStateDescriptor<Double, String>("map-state", Double.class, String.class));
            }

            @Override
            public void flatMap(WaterSensor value, Collector<WaterSensor> out) throws Exception {

                //判断当前数据中的VC是否存在于状态中
                if (!mapState.contains(value.getVc())) {
                    mapState.put(value.getVc(), "1");
                    out.collect(value);
                } else {
                    System.out.println("重复数据！");
                }
            }
        });

        //打印
        resultDS.print(">>>>");

        //启动任务
        env.execute();

    }

}
