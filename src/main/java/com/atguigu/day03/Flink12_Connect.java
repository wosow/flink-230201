package com.atguigu.day03;

import com.alibaba.fastjson.JSONObject;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.ConnectedStreams;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.CoMapFunction;

public class Flink12_Connect {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream0 = env.socketTextStream("hadoop102", 7777);
        SingleOutputStreamOperator<JSONObject> socketTextStream1 = env.socketTextStream("hadoop102", 8888).map(new MapFunction<String, JSONObject>() {
            @Override
            public JSONObject map(String value) throws Exception {
                return JSONObject.parseObject(value);
            }
        });

        //使用Connect连接两个流
        ConnectedStreams<String, JSONObject> connectDS = socketTextStream0.connect(socketTextStream1);
        SingleOutputStreamOperator<String> resultDS = connectDS.map(new CoMapFunction<String, JSONObject, String>() {
            @Override
            public String map1(String value) throws Exception {
                return value;
            }

            @Override
            public String map2(JSONObject value) throws Exception {
                return value.getString("id");
            }
        });

        //打印
        socketTextStream0.print("000>>>");
        socketTextStream1.print("111>>>");
        resultDS.print("resultDS>>>");

        //启动
        env.execute();
    }

}
