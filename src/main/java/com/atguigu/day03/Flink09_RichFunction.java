package com.atguigu.day03;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink09_RichFunction {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);

        //读取端口数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //使用富函数转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new RichMapFunction<String, WaterSensor>() {

            @Override
            public void open(Configuration parameters) throws Exception {
                //资源的创建
                System.out.println("调用Open方法！");
            }

            @Override
            public WaterSensor map(String value) throws Exception {
                System.out.println("调用map方法！");
                String[] words = value.split(",");
                return new WaterSensor(words[0], Long.parseLong(words[1]), Double.parseDouble(words[2]));
            }

            @Override
            public void close() throws Exception {
                //资源的释放
                System.out.println("调用close方法！");
            }
        });

        //打印
        waterSensorDS.print(">>>>");

        //启动
        env.execute();
    }

}
