package com.atguigu.day03;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;
import java.util.List;

public class Flink01_Source_Collection {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //构建集合
        List<Integer> list = Arrays.asList(1, 2, 3);

        //从集合创建流
        DataStreamSource<Integer> streamSource = env.fromCollection(list);

        //打印
        streamSource.print();

        //启动任务
        env.execute();

    }
}
