package com.atguigu.day03;

import com.atguigu.func.MySourceFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink05_Source_Customer {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从自定Source获取数据
        DataStreamSource<String> streamSource = env.addSource(new MySourceFunction());

        //打印
        streamSource.print();

        //启动
        env.execute();

    }

}
