package com.atguigu.day03;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink11_Union {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream0 = env.socketTextStream("hadoop102", 7777);
        DataStreamSource<String> socketTextStream1 = env.socketTextStream("hadoop102", 8888);
        DataStreamSource<String> socketTextStream2 = env.socketTextStream("hadoop102", 9999);

        //使用Union合并两个流
        DataStream<String> unionDS = socketTextStream1.union(socketTextStream0, socketTextStream2);

        //打印
        socketTextStream0.print("000>>>");
        socketTextStream1.print("111>>>");
        socketTextStream2.print("222>>>");
        unionDS.print("unionDS>>>");

        //启动
        env.execute();
    }

}
