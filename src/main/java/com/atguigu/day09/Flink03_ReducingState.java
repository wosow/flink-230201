package com.atguigu.day09;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ReducingState;
import org.apache.flink.api.common.state.ReducingStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink03_ReducingState {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //计算每种传感器的水位和
        SingleOutputStreamOperator<Tuple2<String, Double>> resultDS = keyedStream.map(new RichMapFunction<WaterSensor, Tuple2<String, Double>>() {

            private ReducingState<Double> reducingState;

            @Override
            public void open(Configuration parameters) throws Exception {
                reducingState = getRuntimeContext().getReducingState(new ReducingStateDescriptor<Double>("reduce-state", new ReduceFunction<Double>() {
                    @Override
                    public Double reduce(Double value1, Double value2) throws Exception {
                        return value1 + value2;
                    }
                }, Double.class));
            }

            @Override
            public Tuple2<String, Double> map(WaterSensor value) throws Exception {

                //将数据累加至状态
                reducingState.add(value.getVc());

                //取出状态中的数据
                Double vc = reducingState.get();

                return new Tuple2<>(value.getId(), vc);
            }
        });

        //打印
        resultDS.print();

        //启动
        env.execute();

    }

}
