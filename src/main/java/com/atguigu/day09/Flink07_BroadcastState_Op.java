package com.atguigu.day09;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.streaming.api.datastream.*;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

public class Flink07_BroadcastState_Op {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(3);

        //从端口读取数据
        DataStreamSource<String> socketTextStream1 = env.socketTextStream("hadoop102", 8888);
        DataStreamSource<String> socketTextStream2 = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream1.map(new StringToWaterSensor());

        //将socketTextStream2处理成广播流
        MapStateDescriptor<String, Double> mapStateDescriptor = new MapStateDescriptor<>("map-state", String.class, Double.class);
        //DataStream<String> broadcast1 = socketTextStream2.broadcast();
        BroadcastStream<String> broadcast = socketTextStream2.broadcast(mapStateDescriptor);

        //连接两个流
        BroadcastConnectedStream<WaterSensor, String> connectDS = waterSensorDS.connect(broadcast);

        //处理连接流:根据广播流数据处理主流数据
        OutputTag<String> outputTag = new OutputTag<String>("outPut") {
        };
        SingleOutputStreamOperator<WaterSensor> resultDS = connectDS.process(new BroadcastProcessFunction<WaterSensor, String, WaterSensor>() {

            @Override
            public void processBroadcastElement(String value, BroadcastProcessFunction<WaterSensor, String, WaterSensor>.Context ctx, Collector<WaterSensor> out) throws Exception {

                //获取广播状态
                BroadcastState<String, Double> broadcastState = ctx.getBroadcastState(mapStateDescriptor);

                //将数据放入广播状态
                broadcastState.put("threshold", Double.parseDouble(value));

            }

            @Override
            public void processElement(WaterSensor value, BroadcastProcessFunction<WaterSensor, String, WaterSensor>.ReadOnlyContext ctx, Collector<WaterSensor> out) throws Exception {

                //获取状态信息
                ReadOnlyBroadcastState<String, Double> broadcastState = ctx.getBroadcastState(mapStateDescriptor);
                Double threshold = broadcastState.get("threshold");

                //判断当前数据是否超过阈值,如果超过,则输出报警信息到侧输出流
                if (value.getVc() > threshold) {
                    ctx.output(outputTag, value.getId() + "超过阈值:" + threshold);
                }

                //将数据直接输出到主流
                out.collect(value);
            }
        });

        //打印
        resultDS.print("resultDS>>>>>");
        resultDS.getSideOutput(outputTag).print("SideOutput>>>>>");

        //启动
        env.execute();
    }
}
