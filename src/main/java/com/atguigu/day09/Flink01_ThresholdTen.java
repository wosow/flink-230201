package com.atguigu.day09;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

public class Flink01_ThresholdTen {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //同一个传感器连续两条数据的VC差值超过10,输出报警信息到侧输出流,主流输出原始数据
        OutputTag<String> outputTag = new OutputTag<String>("side") {
        };
        SingleOutputStreamOperator<WaterSensor> processDS = keyedStream.process(new KeyedProcessFunction<String, WaterSensor, WaterSensor>() {

            private ValueState<Double> valueState;

            @Override
            public void open(Configuration parameters) throws Exception {
                valueState = getRuntimeContext().getState(new ValueStateDescriptor<Double>("value-state", Double.class));
            }

            @Override
            public void processElement(WaterSensor value, KeyedProcessFunction<String, WaterSensor, WaterSensor>.Context ctx, Collector<WaterSensor> out) throws Exception {

                //取出状态中的数据
                Double lastVc = valueState.value();
                //取出当前数据中的VC
                Double curVc = value.getVc();
//                if (lastVc == null) {
//                    //更新状态
//                    valueState.update(curVc);
//                } else if (Math.abs(curVc - lastVc) > 10.0D) {
//                    //连续两条数据的VC差值超过10,则输出报警信息到侧输出流
//                    ctx.output(outputTag, value.getId() + "连续两条数据的VC差值超过10！");
//                    //更新状态
//                    valueState.update(curVc);
//                } else {
//                    //更新状态
//                    valueState.update(curVc);
//                }

                if (lastVc != null && Math.abs(curVc - lastVc) > 10.0D) {
                    ctx.output(outputTag, value.getId() + "连续两条数据的VC差值超过10！");
                }

                //更新状态
                valueState.update(curVc);

                //输出原始数据到主流
                out.collect(value);
            }
        });

        //打印数据
        processDS.print("processDS>>>>");
        processDS.getSideOutput(outputTag).print("SideOut>>>>>");

        //启动任务
        env.execute();

    }
}
